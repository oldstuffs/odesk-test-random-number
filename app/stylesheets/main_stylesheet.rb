class MainStylesheet < ApplicationStylesheet

  def setup
    # Add sytlesheet specific setup stuff here.
    # Add application specific setup stuff in application_stylesheet.rb
  end

  def root_view(st)
    st.background_color = color.white
  end

  def hello_world(st)
    st.frame          = {top: 100, width: 380, height: 18, centered: :horizontal}
    st.text_alignment = :center
    st.color          = color.battleship_gray
    st.font           = font.small
  end

  def standard_button(st)
    st.background_color = color.tint
    st.color            = color.white
    st.frame            = {l: 20, w: (st.superview.size.width - 40), fb:25, h: 50}
  end
  def backup_btn(st)
    standard_button st
    st.frame = {t:150}
    st.text  = "Backup"
  end
  def restore_btn(st)
    standard_button st
    st.frame = {t:230}
    st.text  = "Restore"
  end

end
