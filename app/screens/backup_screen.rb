class BackupScreen < PM::Screen
  title "Backup"

  def on_load
    # Sets a top of 0 to be below the navigation control, it's best not to do this
    # self.edgesForExtendedLayout = UIRectEdgeNone

    rmq.stylesheet = MainStylesheet
    rmq(self.view).apply_style :root_view

    # Create your UIViews here
    @hello_world_label = rmq.append(UILabel, :hello_world).get

    rmq.append(UIButton, :backup_btn).on(:touch) { backup_func }
    rmq.append(UIButton, :restore_btn).on(:touch) { restore_func }

    if App::Persistence['channels']
      @hello_world_label.text = App::Persistence['channels']
    end
  end

  def backup_func
    local_backup_path = NSHomeDirectory().stringByAppendingPathComponent( "Documents/backup.plist" )
    icloud = NSFileManager.defaultManager.URLForUbiquityContainerIdentifier("iCloud.com.chunlea.odesk")

    if icloud
      icloud_path = icloud.URLByAppendingPathComponent("Documents/backup.plist")
      NSLog("icloud path: %@", icloud_path)
      backup_to icloud_path.path do |result|
        if result
          App.alert("Backup to iCloud successful")
        else
          backup_to local_backup_path do |result|
            if result
              App.alert("Backup to iCloud failed, try to backup to local successful")
            else
              App.alert("Backup to iCloud failed, try to backup to local failed")
            end
          end
        end
      end
    else
      backup_to local_backup_path do |result|
        if result
          App.alert("iCloud not woking on your device, backup to local successful")
        else
          App.alert("iCloud not woking on your device, backup to local failed")
        end
      end
    end
  end

  def backup_to(path, &block)
    data = NSMutableArray.alloc.initWithArray(Widget.map {|i| [i.number, i.id]})

    if data.writeToFile(path, atomically:true)
      App::Persistence['channels'] = Time.now.strftime("Last backup %m/%d/%Y %I:%M%p.")
      @hello_world_label.text = App::Persistence['channels']
      block.call(true) if block
      puts NSMutableDictionary.dictionaryWithContentsOfFile(path)
    else
      block.call(false) if block
    end
  end

  def restore_func
    local_backup_path = NSHomeDirectory().stringByAppendingPathComponent( "Documents/backup.plist" )
    icloud = NSFileManager.defaultManager.URLForUbiquityContainerIdentifier("iCloud.com.chunlea.odesk")

    if icloud
      icloud_path = icloud.URLByAppendingPathComponent("Documents/backup.plist")
      restore_from icloud_path.path do |result|
        if result
          App.alert("Restore from iCloud successful")
        else
          restore_from local_backup_path do |result|
            if result
              App.alert("NO backup in iCloud, Restore for local successful")
            else
              App.alert("NO backup in iCloud, Restore for local failed")
            end
          end
        end
      end
    else
      restore_from local_backup_path do |result|
        if result
          App.alert("NO backup in iCloud, Restore for local successful")
        else
          App.alert("NO backup in iCloud, Restore for local failed")
        end
      end
    end
  end

  def restore_from(path, &block)
    if NSFileManager.defaultManager.fileExistsAtPath(path)
      Widget.destroy_all
      data = NSArray.alloc.initWithContentsOfFile(path)
      data.each do |d|
        Widget.create(number: d[0], id: d[1])
      end
      cdq.save
      block.call(true) if block
    else
      block.call(false) if block
    end
  end

  # Remove these if you are only supporting portrait
  def supportedInterfaceOrientations
    UIInterfaceOrientationMaskAll
  end
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    # Called before rotation
    rmq.all.reapply_styles
  end
  def viewWillLayoutSubviews
    # Called anytime the frame changes, including rotation, and when the in-call status bar shows or hides
    #
    # If you need to reapply styles during rotation, do it here instead
    # of willAnimateRotationToInterfaceOrientation, however make sure your styles only apply the layout when
    # called multiple times
  end
  def didRotateFromInterfaceOrientation(from_interface_orientation)
    # Called after rotation
  end
end


__END__

# You don't have to reapply styles to all UIViews, if you want to optimize,
# another way to do it is tag the views you need to restyle in your stylesheet,
# then only reapply the tagged views, like so:
def logo(st)
  st.frame = {t: 10, w: 200, h: 96}
  st.centered = :horizontal
  st.image = image.resource('logo')
  st.tag(:reapply_style)
end

# Then in willAnimateRotationToInterfaceOrientation
rmq(:reapply_style).reapply_styles


