class MainScreen < PM::TableScreen
  title "Table"

  def on_load
    # Sets a top of 0 to be below the navigation control, it's best not to do this
    # self.edgesForExtendedLayout = UIRectEdgeNone

    rmq.stylesheet = MainStylesheet
    rmq(self.view).apply_style :root_view

    if Widget.count == 0
      seed_func
    end
    @widgets = []
    load_async

    set_nav_bar_button :right, title: "Seed", action: :seed_func
  end

  def table_data
    [{
      cells: @widgets.map do |widget|
        {
          title: widget.number,
        }
      end
    }]
  end

  def will_appear
    load_async
  end

  def load_async
    @widgets = Widget.sort_by(:id).to_a
    update_table_data
  end

  def seed_func
    Widget.destroy_all
    10.times do |i|
      Widget.create(id: i, number: Random.new().rand(100000...1000000).to_s)
    end
    cdq.save
    load_async
  end

  # Remove these if you are only supporting portrait
  def supportedInterfaceOrientations
    UIInterfaceOrientationMaskAll
  end
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    # Called before rotation
    rmq.all.reapply_styles
  end
  def viewWillLayoutSubviews
    # Called anytime the frame changes, including rotation, and when the in-call status bar shows or hides
    #
    # If you need to reapply styles during rotation, do it here instead
    # of willAnimateRotationToInterfaceOrientation, however make sure your styles only apply the layout when
    # called multiple times
  end
  def didRotateFromInterfaceOrientation(from_interface_orientation)
    # Called after rotation
  end
end


__END__

# You don't have to reapply styles to all UIViews, if you want to optimize,
# another way to do it is tag the views you need to restyle in your stylesheet,
# then only reapply the tagged views, like so:
def logo(st)
  st.frame = {t: 10, w: 200, h: 96}
  st.centered = :horizontal
  st.image = image.resource('logo')
  st.tag(:reapply_style)
end

# Then in willAnimateRotationToInterfaceOrientation
rmq(:reapply_style).reapply_styles


